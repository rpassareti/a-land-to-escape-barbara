﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentScript : MonoBehaviour
{
    [SerializeField]
    private int choseDirection;
    [SerializeField]
    private float posX, posY, posZ;
    [SerializeField]
    private float posXini, posYini, posZini;
    [SerializeField]
    private float speed;

    private Quaternion qTo;

    public bool moving;

    void Awake()
    {
        choseDirection = Random.Range(0, 3);
        posXini = transform.position.x;
        posYini = transform.position.y;
        posZini = transform.position.z;

        posX = Random.Range(transform.position.x+10, transform.position.x + 50);
        posY = Random.Range(transform.position.y+10, transform.position.y + 50);
        posZ = Random.Range(transform.position.z+10, transform.position.z + 50);

        speed = Random.Range(0f, 1f);
    }

    private void Update()
    {
        switch (choseDirection)
        {
            case 0:
                if (!moving)
                {
                    transform.position = Vector3.Lerp(transform.position, new Vector3(posX, transform.position.y, transform.position.z), Time.deltaTime * speed);

                    Vector3 relativeWayPointPos = transform.InverseTransformPoint(new Vector3(posX, transform.position.y, transform.position.z));

                    if ((relativeWayPointPos.magnitude < 1f))
                    {
                        moving = true;
                    }
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, new Vector3(posXini, transform.position.y, transform.position.z), Time.deltaTime * speed);

                    Vector3 relativeWayPointPos = transform.InverseTransformPoint(new Vector3(posXini, transform.position.y, transform.position.z));

                    if ((relativeWayPointPos.magnitude < 1f))
                    {
                        moving = false;
                    }
                }

                break;

            case 1:
                if (!moving)
                {
                    transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, posY, transform.position.z), Time.deltaTime * speed);

                    Vector3 relativeWayPointPos = transform.InverseTransformPoint(new Vector3(transform.position.x, posY, transform.position.z));

                    if ((relativeWayPointPos.magnitude < 1f))
                    {
                        moving = true;
                    }
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, posYini, transform.position.z), Time.deltaTime * speed);

                    Vector3 relativeWayPointPos = transform.InverseTransformPoint(new Vector3(transform.position.x, posYini, transform.position.z));

                    if ((relativeWayPointPos.magnitude < 1f))
                    {
                        moving = false;
                    }
                }
                break;

            case 2:
                if (!moving)
                {
                    transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, posZ), Time.deltaTime * speed);

                    Vector3 relativeWayPointPos = transform.InverseTransformPoint(new Vector3(transform.position.x, transform.position.y, posZ));

                    if ((relativeWayPointPos.magnitude < 1f))
                    {
                        moving = true;
                    }
                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, posZini), Time.deltaTime * speed);

                    Vector3 relativeWayPointPos = transform.InverseTransformPoint(new Vector3(transform.position.x, transform.position.y, posZini));

                    if ((relativeWayPointPos.magnitude < 1f))
                    {
                        moving = false;
                    }
                }
                break;
        }
        transform.Rotate(posX * speed, posY * speed, posZ * speed);
    }
}
