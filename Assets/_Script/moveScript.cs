﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class moveScript : MonoBehaviour {

    public GameObject player, panelDes, panelActv, movePositionUpDown;
    public GameObject[] movePosition;
    public Material sphereGlobe;
    public bool actve, desactv, updown;
    public float vel;
    public int i, oldI = -1, timeChangeBG;

    public Color color;
    public AudioClip fundoRapido, fundoNormal;

    private void OnEnable()
    {
        GetComponent<Button>().enabled = true;
        GetComponent<Image>().enabled = true;
    }

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        sphereGlobe = GameObject.Find("GLOBO360").GetComponent<MeshRenderer>().material;
    }

    public void ActiveClick()
    {
        actve = true;
        i = Random.Range(0, movePosition.Length);
        if(oldI == -1)
        {
            oldI = i;
        }
        else
        {
            if(i == oldI)
            {
                ActiveClick();
            }
            else
            {
                oldI = i;
            }
        }

    }

    public void ChangeUpDown()
    {
        updown = true;
    }

    public void ChangeBackgroundColor()
    {
        StartCoroutine(BackgroundColor());
    }

    IEnumerator BackgroundColor()
    {
        float timer = 0;
        float clrInt;
        GameObject.Find("GLOBO360").GetComponent<AudioSource>().clip = fundoRapido;
        GameObject.Find("GLOBO360").GetComponent<AudioSource>().Play();
        while (timer < timeChangeBG)
        {
            yield return new WaitForSeconds(0.05f);
            timer += Time.deltaTime;
            clrInt = Random.Range(0f, 1f);
            color = new Color(clrInt, clrInt, clrInt, 255);
            sphereGlobe.color = color;
            Debug.Log(timer);
            Debug.Log(sphereGlobe.color);
        }

        GameObject.Find("Sphere_Change").GetComponent<changeMesh>().ChangeColor(false);
        yield return new WaitForSeconds(0f);
        GameObject.Find("GLOBO360").GetComponent<AudioSource>().clip = fundoNormal;
        GameObject.Find("GLOBO360").GetComponent<AudioSource>().Play();
    }

    public void DesactvObj()
    {
        desactv = true;
    }

    private void Update()
    {
        if (actve)
        {
            //GetComponent<Button>().enabled = false;
            //GetComponent<Image>().enabled = false;
            
            player.transform.localPosition = Vector3.Lerp(player.transform.position, movePosition[i].transform.position, Time.deltaTime * vel);

            if (Vector3.Distance(player.transform.position, movePosition[i].transform.position) < 0.01f)
            {
                actve = false;
                player.transform.position = movePosition[i].transform.position;
                if (desactv)
                {
                    desactv = false;
                    panelDes.SetActive(false);
                    panelActv.SetActive(true);
                }
                //this.gameObject.SetActive(false);
            }
        }
        if(updown)
        {
            player.transform.position = movePositionUpDown.transform.position;
            player.transform.rotation = movePositionUpDown.transform.rotation;
            if (desactv)
            {
                desactv = false;
                panelDes.SetActive(false);
                panelActv.SetActive(true);
            }
            updown = false;
            //this.gameObject.SetActive(false);
        }
    }
}
