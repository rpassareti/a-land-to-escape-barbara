﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeMesh : MonoBehaviour {

    private int i = 0;
    [SerializeField]
    private int maxSpawn = 0;
    private bool startApp = true;
    private bool oneTime = true;

    private Mesh mesh;
    private Vector3[] vertices;


    public GameObject[] particles;
    public GameObject buttons;
    public GameObject spawnObject;
    public GameObject[] audioObj;

    public float g = 0;
    public float timer;
    public float totalTimer;

    public int distance;

    // Use this for initialization
    void Start ()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        //Loop();
        sphereGlobe = GameObject.Find("GLOBO360").GetComponent<MeshRenderer>().material;
        StartCoroutine(BackgroundColorLoop());
	}

    void Loop()
    {
        //for (y = 0; y < qntVezes; y++)
        //{
            i = Random.Range(0, vertices.Length);
            vertices[i] += Vector3.RotateTowards(vertices[i], transform.position, 1, 1) * Time.deltaTime * g * 1;
            mesh.vertices = vertices;
            mesh.RecalculateBounds();
        //}
    }

    IEnumerator BeginApp()
    {
        oneTime = false;
        for (int y = 0; y < particles.Length; y++)
        {
            particles[y].SetActive(true);
            yield return new WaitForSeconds(1f);
        }

        for (int y = 0; y <= maxSpawn; y++)
        {
            Vector3 pos = Random.onUnitSphere * distance;
            GameObject go = Instantiate(spawnObject, pos, transform.rotation);
        }

        for (int y = 0; y < audioObj.Length; y++)
        {
            audioObj[y].GetComponent<AudioSource>().Play();
        }

            startApp = false;
    }

    private void Update()
    {
        if (startApp)
        {
            if (oneTime)
            {
                StartCoroutine(BeginApp());
            }
        }
        else
        {
            if (timer < totalTimer)
            {
                Loop();
                timer += Time.deltaTime;
            }
            else
            {
                buttons.SetActive(true);
            }
        }

    }

    public Color color;
    public int timeChangeBG;
    public Material sphereGlobe;
    public bool desActv;

    IEnumerator BackgroundColorLoop()
    {
        if (!desActv)
        {
            float clrInt;
            yield return new WaitForSeconds(0.5f);
            clrInt = Random.Range(0, 1f);
            color = new Color(clrInt, clrInt, clrInt, 255);
            sphereGlobe.color = color;
            StartCoroutine(BackgroundColorLoop());
        }
    }

    public void ChangeColor(bool actv)
    {
        desActv = actv;
        StartCoroutine(BackgroundColorLoop());
    }
}
